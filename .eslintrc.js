module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  extends: [
    "eslint:recommended",
    "airbnb-typescript",
    "prettier",
    "prettier/@typescript-eslint",
  ],
}
