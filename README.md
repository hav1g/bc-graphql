# BigCommerce GraphQL

### Steps to run

1. `yarn install`
2. `yarn dev` will fire up a server on `:4000`
3. You can visit `localhost:4000` and make queries with the provided GraphQL Playground
