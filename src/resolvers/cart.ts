import { camelCase, mapKeys } from "lodash"
import { UpdateOperation, StatusCodes } from "../lib/constants"

interface LineItem {
  id: string
  variant_id: number
  product_id: number
  quantity: number
  name: string
  extended_list_price: number
  extended_sale_price: number
  list_price: number
  options: Array<{ value: string }>
  sale_price: number
  image_url: string
  sku: string
}

interface TransformedLineItem {
  id: string
  variantId: number
  productId: number
  quantity: number
  name: string
  variantName: string
  extendedListPrice: number
  extendedSalePrice: number
  listPrice: number
  salePrice: number
  imageUrl: string
  sku: string
}

interface RedirectUrls {
  [k: string]: string
}

interface Cart {
  id: string
  subTotal: number
  total: number
  items: TransformedLineItem[]
  status?: Status.DELETED | string
  error?: StatusError
  redirectUrls?: RedirectUrls
}

interface StatusError {
  code: number
  title: string
}

enum Status {
  DELETED = "DELETED",
}

function mapLineItems<T extends LineItem>(
  lineItems: T[],
): TransformedLineItem[] {
  if (!lineItems) {
    return []
  }

  return lineItems.map((item: T) => ({
    id: item.id,
    variantId: item.variant_id,
    productId: item.product_id,
    quantity: item.quantity,
    name: item.name,
    variantName: item.options?.[0]?.value || "",
    extendedListPrice: item.extended_list_price,
    extendedSalePrice: item.extended_sale_price,
    listPrice: item.list_price,
    salePrice: item.sale_price,
    imageUrl: item.image_url,
    sku: item.sku,
  }))
}

export const cartResolver = {
  Mutation: {
    createCart,
    updateCart,
    updateItemInCart,
    createCheckoutRedirects,
  },
  Query: {
    getCart,
  },
}

async function createCart(
  _root: any,
  { lineItems },
  { dataSources },
): Promise<Cart> {
  const { data } = await dataSources.cartAPI.createCart({ lineItems })

  return {
    id: data?.id,
    subTotal: data?.base_amount,
    total: data?.cart_amount,
    items: mapLineItems(data?.line_items?.physical_items),
    redirectUrls: {
      cartUrl: data?.redirect_urls?.cart_url,
      checkoutUrl: data?.redirect_urls?.checkout_url,
      embeddedCheckoutUrl: data?.redirect_urls?.embedded_checkout_url,
    },
  }
}

async function getCart(
  _root: any,
  { cartId },
  { dataSources },
): Promise<Partial<Cart>> {
  const { data } = await dataSources.cartAPI.getCart({
    cartId,
  })

  if (data?.code === StatusCodes.NOT_FOUND) {
    return {
      error: {
        code: data.code,
        title: data.title,
      },
    }
  }

  return {
    id: data.id,
    subTotal: data.base_amount,
    total: data.cart_amount,
    items: mapLineItems(data?.line_items?.physical_items),
  }
}

async function updateCart(
  _root: any,
  { cartId, lineItems, operation },
  { dataSources },
): Promise<Partial<Cart>> {
  const { data } = await dataSources.cartAPI.updateCart(operation, {
    cartId,
    lineItems,
  })

  if (operation === UpdateOperation.DELETE && !data) {
    return {
      status: Status.DELETED,
    }
  }

  return {
    id: data.id,
    subTotal: data.base_amount,
    total: data.cart_amount,
    items: mapLineItems(data?.line_items?.physical_items),
  }
}

async function updateItemInCart(
  _root: any,
  { cartId, lineItem, operation },
  { dataSources },
): Promise<Partial<Cart>> {
  const { data } = await dataSources.cartAPI.updateItemInCart(operation, {
    cartId,
    lineItem,
  })

  if (operation === UpdateOperation.DELETE && !data) {
    return {
      status: Status.DELETED,
    }
  }

  // NOTE: This should be called when adding to the quantity of an item, but the
  // desired qty is more than the inventory level
  if (
    operation === UpdateOperation.UPDATE &&
    data?.code === StatusCodes.MISSING_OR_INVALID_DATA
  ) {
    return {
      error: {
        code: data.code,
        title: data.title,
      },
    }
  }

  return {
    id: data.id,
    subTotal: data.base_amount,
    total: data.cart_amount,
    items: mapLineItems(data?.line_items?.physical_items),
  }
}

async function createCheckoutRedirects(
  _root: any,
  { cartId },
  { dataSources },
): Promise<RedirectUrls> {
  const { data } = await dataSources.cartAPI.createCheckoutRedirects({
    cartId,
  })

  return mapKeys(data, (_v, k) => camelCase(k))
}
