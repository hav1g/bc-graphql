import { camelCase, mapKeys } from "lodash"
import jwt from "jsonwebtoken"
import { v4 } from "uuid"

import config from "config"

enum Status {
  ERROR,
  OK,
}

export const accountResolver = {
  Mutation: {
    createAccount,
    createAccountAddress,
    handleAccountLogin,
  },
}

async function createAccount(_root: any, { account }, { dataSources }) {
  const { data } = await dataSources.bcAPI.createAccount({ account })

  return mapKeys(data?.[0], (_v, k) => camelCase(k))
}

async function createAccountAddress(_root: any, { address }, { dataSources }) {
  const { data } = await dataSources.bcAPI.createAccountAddress({ address })

  if (data?.[0]?.id) {
    return {
      status: Status.OK,
    }
  }

  return {
    status: Status.ERROR,
  }
}

async function handleAccountLogin(
  _root: any,
  { email, password },
  { dataSources },
) {
  if (!email && !password) {
    return {
      status: "WRONG",
    }
  }

  const { data } = await dataSources.bcAPI.getAllCustomers({ email })
  const { data: time } = await dataSources.bcAPIv2.getTime()
  const customerId = data?.[0]?.id
  const dateCreated = Math.round(time / 1000)
  const payload = {
    iss: config.get("client_id"),
    iat: dateCreated,
    jti: v4(),
    operation: "customer_login",
    store_hash: config.get("store_hash"),
    customer_id: customerId,
  }
  const token = jwt.sign(payload, config.get("store_token"), {
    algorithm: "HS256",
  })

  return {
    url: `https://vapesynth.mybigcommerce.com/login/token/${token}`,
  }
}
