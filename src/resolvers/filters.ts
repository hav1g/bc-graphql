import { ELiquidFilters } from "../util/constants"

export const filtersResolver = {
  Query: {
    eLiquidFilters,
  },
  FilterTuple: {
    name: ({ name }) => name,
    id: ({ id }) => id,
  },
}

async function eLiquidFilters(_root: never, _args: never, { dataSources }) {
  const { data: strength } = await dataSources.bcAPI.getCategories({
    parentId: ELiquidFilters.STRENGTH,
  })
  const { data: sizes } = await dataSources.bcAPI.getCategories({
    parentId: ELiquidFilters.SIZES,
  })
  const { data: flavor } = await dataSources.bcAPI.getCategories({
    parentId: ELiquidFilters.FLAVOR,
  })

  return [
    {
      displayName: "Flavor",
      filters: flavor,
    },
    {
      displayName: "Sizes",
      filters: sizes,
    },
    {
      displayName: "Strength",
      filters: strength,
    },
  ]
}
