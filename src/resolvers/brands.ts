import { camelCase } from "lodash"
import mapKeysDeep from "../util/deepMap"

interface Brand {
  id: number
  imageUrl: string
  name: string
}

export const brandsResolver = {
  Query: {
    brands,
  },
}

async function brands(_root: any, _args: never, { dataSources }) {
  const { data } = await dataSources.bcAPI.getBrands()

  const brands = data.sort((a: Brand, b: Brand) => (a.name < b.name ? -1 : 1))

  return {
    edges: brands.map((b: any) => ({
      node: {
        ...mapKeysDeep(b, (_v, k) => camelCase(k)),
        url: b.custom_url?.url,
        description: b.meta_description,
      },
    })),
  }
}
