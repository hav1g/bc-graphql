import { camelCase } from "lodash"

import mapKeysDeep from "../util/deepMap"
import { Categories } from "../util/constants"

export const frontPageResolver = {
  Query: {
    frontPage,
  },
  Product: {
    id: ({ id }) => id,
    name: ({ name }) => name,
    prices: ({ prices }) => prices,
  },
}

async function frontPage(_root: never, _args: never, { dataSources }) {
  const { data: featured } = await dataSources.bcAPI.getProducts({
    limit: 6,
    categoriesIn: [Categories.FEATURED],
    include: ["images"],
  })
  const { data: newest } = await dataSources.bcAPI.getProducts({
    limit: 6,
    include: ["images"],
    sort: "date_modified",
  })

  return {
    featured: {
      edges: featured.map((d: any) => ({
        node: {
          ...mapKeysDeep(d, (_v, k) => camelCase(k)),
          path: d?.["custom_url"]?.["url"],
          prices: {
            price: d?.price,
            retailPrice: d?.["retail_price"],
            salePrice: d?.["sale_price"],
          },
        },
      })),
    },
    newest: {
      edges: newest.map((d: any) => ({
        node: {
          ...mapKeysDeep(d, (_v, k) => camelCase(k)),
          path: d?.["custom_url"]?.["url"],
          prices: {
            price: d?.price,
            retailPrice: d?.["retail_price"],
            salePrice: d?.["sale_price"],
          },
        },
      })),
    },
  }
}
