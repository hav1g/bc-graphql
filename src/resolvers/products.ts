import { camelCase } from "lodash"
import mapKeysDeep from "../util/deepMap"

type AnyObject = { [k: string]: any }

export const productsResolver = {
  Query: {
    products,
    product,
    relatedProducts,
  },
  // Minimum requirements for properties that must appear on the repsonse
  Product: {
    id: ({ id }) => id,
    name: ({ name }) => name,
    prices: ({ prices }) => prices,
  },
}

async function products(
  _root: any,
  { categoriesIn, price, brandId, offset = 0, limit = 12 },
  { dataSources },
): Promise<AnyObject> {
  const { data, meta } = await dataSources.bcAPI.getProducts({
    // need to handle what happens when this is > 50 since that's the maximum size per res
    // will involve incrementing to the next page in the req and passing in
    limit: offset ? limit + offset : limit,
    categoriesIn,
    brandId,
    price,
    include: ["images"],
  })

  const products = data.slice(offset, offset + limit)

  return {
    edges: products.map((d: any) => ({
      node: {
        ...mapKeysDeep(d, (_v, k) => camelCase(k)),
        isInStock: d?.["inventory_level"] > 0,
        path: d?.["custom_url"]?.["url"],
        prices: {
          price: d?.price,
          retailPrice: d?.["retail_price"],
          salePrice: d?.["sale_price"],
        },
      },
    })),
    pageInfo: {
      offset: offset + limit,
      limit,
      totalPages: Math.ceil(meta.pagination.total / limit),
    },
  }
}

async function product(
  _root: any,
  { id },
  { dataSources },
): Promise<AnyObject> {
  const { data } = await dataSources.bcAPI.getProduct({
    id,
    include: ["images", "variants"],
  })

  return {
    edges: [
      {
        node: {
          ...mapKeysDeep(data, (_v, k) => camelCase(k)),
          isInStock: data?.["inventory_level"] > 0,
          path: data?.["custom_url"]?.["url"],
          prices: {
            price: data?.price,
            retailPrice: data?.retailPrice,
            salePrice: data?.salePrice,
          },
          ...(data?.variants
            ? {
                variants: {
                  displayName:
                    data.variants[0]?.option_values?.[0]?.option_display_name,
                  values: data.variants.map((v) => ({
                    id: v.id,
                    name: v.option_values[0]?.label,
                    inventoryLevel: v.inventory_level,
                    inventoryWarningLevel: v.inventory_warning_level,
                  })),
                },
              }
            : {}),
        },
      },
    ],
  }
}

async function relatedProducts(
  _root: any,
  { id, limit },
  { dataSources },
): Promise<AnyObject> {
  const { data: products } = await dataSources.bcAPI.getProducts({
    limit,
    categoriesIn: [id],
    include: ["images"],
  })

  return {
    edges: products.map((d: any) => ({
      node: {
        ...mapKeysDeep(d, (_v, k) => camelCase(k)),
        isInStock: d?.["inventory_level"] > 0,
        path: d?.["custom_url"]?.["url"],
        prices: {
          price: d?.price,
          retailPrice: d?.["retail_price"],
          salePrice: d?.["sale_price"],
        },
      },
    })),
  }
}
