import { ApolloServer } from "apollo-server"
import config from "config"

import { BigCommerceAPI } from "./datasource"
import { BigCommerceAPIV2 } from "./datasourcev2"
import { CartAPI } from "./cartDatasource"
import schema from "./schemas"

const server = new ApolloServer({
  schema,
  dataSources: () => ({
    bcAPI: new BigCommerceAPI(config.get("api")),
    bcAPIv2: new BigCommerceAPIV2(config.get("v2")),
    cartAPI: new CartAPI(config.get("api")),
  }),
})

server.listen().then(({ url }) => console.log(`Server ready at ${url}`))
