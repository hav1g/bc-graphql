export enum UpdateOperation {
  UPDATE = "UPDATE",
  DELETE = "DELETE",
}

export enum StatusCodes {
  MISSING_OR_INVALID_DATA = 422,
  NOT_FOUND = 404,
}
