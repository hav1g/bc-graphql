import { gql } from "apollo-server"

export const Brands = gql`
  type Brand {
    id: Int!
    imageUrl: String
    name: String!
    url: String!
    description: String
  }

  type BrandConnection {
    node: Brand!
  }

  type BrandEdges {
    edges: [BrandConnection]
  }
`
