import { gql } from "apollo-server"

export const StatusError = gql`
  type StatusError {
    code: Int!
    title: String!
  }
`

export const Status = gql`
  type Status {
    status: Int
  }
`
