import { gql } from "apollo-server"

export const Queries = gql`
  type Query {
    eLiquidFilters: [Filter]!
    frontPage: FrontPage!
    products(
      categoriesIn: [Int]
      price: FilterPrice
      brandId: Int
      offset: Int
      limit: Int
      type: String
    ): ProductEdges!
    product(id: Int!): ProductEdges!
    relatedProducts(id: Int!, limit: Int!): ProductEdges!
    getCart(cartId: String!): Cart
    brands: BrandEdges!
  }
`
