import { gql } from "apollo-server"

export const Filters = gql`
  type FilterTuple {
    name: String!
    id: Int!
  }

  type Filter {
    displayName: String
    filters: [FilterTuple]!
  }
`
