import { gql } from "apollo-server"

export const Mutations = gql`
  enum UpdateOperation {
    UPDATE
    DELETE
  }

  type Mutation {
    createAccount(account: CreateAccount): Account
    createAccountAddress(address: Address): Status
    createCart(lineItems: [Item!]): Cart
    updateCart(
      cartId: String!
      lineItems: [Item]
      operation: UpdateOperation!
    ): Cart
    updateItemInCart(
      cartId: String!
      lineItem: Item
      operation: UpdateOperation!
    ): Cart
    handleAccountLogin(email: String!, password: String!): LoginUrl
    createCheckoutRedirects(cartId: String!): RedirectUrls
  }
`
