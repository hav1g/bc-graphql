import { makeExecutableSchema } from "apollo-server"
import { merge } from "lodash"

import { accountResolver } from "../resolvers/account"
import { brandsResolver } from "../resolvers/brands"
import { cartResolver } from "../resolvers/cart"
import { filtersResolver } from "../resolvers/filters"
import { frontPageResolver } from "../resolvers/front-page"
import { productsResolver } from "../resolvers/products"

import { Account } from "./account"
import { Brands } from "./brands"
import { Cart } from "./cart"
import { RedirectUrls } from "./redirectUrls"
import { Enums } from "./enums"
import { Filters } from "./filters"
import { FrontPage } from "./frontPage"
import { Inputs } from "./inputs"
import { Mutations } from "./mutations"
import { PageInfo } from "./pageInfo"
import { Prices } from "./prices"
import { Products } from "./products"
import { Queries } from "./queries"
import { Status, StatusError } from "./status"

export default makeExecutableSchema({
  typeDefs: [
    Account,
    Brands,
    Cart,
    RedirectUrls,
    Enums,
    Filters,
    FrontPage,
    Inputs,
    Mutations,
    PageInfo,
    Prices,
    Products,
    Queries,
    Status,
    StatusError,
  ],
  resolvers: merge(
    accountResolver,
    brandsResolver,
    cartResolver,
    filtersResolver,
    frontPageResolver,
    productsResolver,
  ),
})
