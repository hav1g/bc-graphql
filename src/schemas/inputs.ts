import { gql } from "apollo-server"

export const Inputs = gql`
  input Authentication {
    new_password: String!
    force_password_reset: Boolean
  }

  input FilterPrice {
    min: Int
    max: Int
  }

  input Item {
    id: String
    quantity: Int
    product_id: Int
    variant_id: Int
  }

  input CreateAccount {
    last_name: String!
    first_name: String!
    email: String!
    authentication: Authentication!
  }

  input Address {
    first_name: String!
    last_name: String!
    address1: String!
    address2: String
    city: String!
    state_or_province: String!
    postal_code: String!
    country_code: String!
    phone: String
    address_type: String
    customer_id: Int!
  }
`
