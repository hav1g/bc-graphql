import { gql } from "apollo-server"

export const RedirectUrls = gql`
  type RedirectUrls {
    cartUrl: String
    checkoutUrl: String
    embeddedCheckoutUrl: String
  }
`
