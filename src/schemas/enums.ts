import { gql } from "apollo-server"

export const Enums = gql`
  enum Availability {
    available
    disabled
    preorder
  }
`
