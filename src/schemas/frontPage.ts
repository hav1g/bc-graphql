import { gql } from "apollo-server"

export const FrontPage = gql`
  type FrontPage {
    featured: ProductEdges!
    newest: ProductEdges!
  }
`
