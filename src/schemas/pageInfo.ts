import { gql } from "apollo-server"

export const PageInfo = gql`
  type PageInfo {
    offset: Int!
    limit: Int!
    totalPages: Int!
  }
`
