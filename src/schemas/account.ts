import { gql } from "apollo-server"

export const Account = gql`
  type Account {
    email: String
    id: Int
  }

  type LoginUrl {
    url: String
  }
`
