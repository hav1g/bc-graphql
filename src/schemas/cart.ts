import { gql } from "apollo-server"

export const Cart = gql`
  type LineItem {
    id: String!
    variantId: Int!
    productId: Int!
    quantity: Int!
    name: String!
    variantName: String
    extendedListPrice: Int!
    extendedSalePrice: Int
    listPrice: Int
    salePrice: Int
    imageUrl: String
    sku: String
  }

  type Cart {
    id: String
    subTotal: Int
    total: Int
    items: [LineItem]
    status: String
    error: StatusError
  }
`
