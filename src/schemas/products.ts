import { gql } from "apollo-server"

export const Products = gql`
  type Product {
    availability: Availability
    brandId: Int
    categories: [Int]
    description: String
    id: Int!
    images: [Image]
    isInStock: Boolean
    name: String!
    options: [Option]
    path: String!
    prices: Prices!
    sku: String
    variants: Variant
    inventoryLevel: Int!
    inventoryWarningLevel: Int!
  }

  type Variant {
    displayName: String
    values: [VariantValue]
  }

  type VariantValue {
    id: Int!
    name: String
    inventoryLevel: Int!
    inventoryWarningLevel: Int!
  }

  type Option {
    id: Int
    displayName: String
    optionValues: [OptionValue]
  }

  type OptionValue {
    id: Int
    label: String!
  }

  type Image {
    id: Int!
    isThumbnail: Boolean
    description: String
    urlStandard: String
    urlThumbnail: String
  }

  type ProductConnection {
    node: Product!
  }

  type ProductEdges {
    edges: [ProductConnection!]
    pageInfo: PageInfo!
  }
`
