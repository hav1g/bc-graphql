import { gql } from "apollo-server"

export const Prices = gql`
  type Prices {
    price: Float!
    retailPrice: Float
    salePrice: Float
  }
`
