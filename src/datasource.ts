import { RequestOptions, RESTDataSource } from "apollo-datasource-rest"
import { URLSearchParamsInit } from "apollo-server-env"

import config from "config"

type Includes = "images" | "variants"

type Sort =
  | "id"
  | "name"
  | "sku"
  | "price"
  | "date_modified"
  | "date_last_imported"
  | "inventory_level"
  | "is_visible"
  | "total_sold"

interface AllProducts {
  limit?: number
  page?: number
  categoriesIn?: number[]
  includeFields?: string[]
  include?: Array<Includes>
  min?: number
  max?: number
  sort?: Sort
  brandId?: number
}

interface SingleProduct {
  id: number
  include?: Array<Includes>
}

interface Account {
  account: {
    last_name: string
    first_name: string
    email: string
    authentication: {
      new_password: string
      force_password_reset?: boolean
    }
  }
}

export class BigCommerceAPI extends RESTDataSource {
  constructor(baseUrl: string) {
    super()
    this.baseURL = baseUrl
  }

  willSendRequest(req: RequestOptions): void {
    req.headers.set("x-auth-token", config.get("token"))
  }

  async getProducts<TResult = any>({
    limit = 50,
    categoriesIn,
    includeFields,
    include,
    min,
    max,
    sort,
    brandId,
  }: AllProducts): Promise<TResult> {
    const params: URLSearchParamsInit = {
      limit,
      ...(includeFields ? { include_fields: includeFields.join(",") } : {}),
      ...(include ? { include: include.join(",") } : {}),
      ...(categoriesIn ? { "categories:in": categoriesIn.join(",") } : {}),
      ...(max ? { "price:min": min ?? 0, "price:max": max } : {}),
      ...(min && !max ? { "price:min": min } : {}),
      ...(sort ? { sort, direction: "desc" } : {}),
      ...(brandId ? { brand_id: brandId } : {}),
    }

    return this.get("catalog/products", params)
  }

  async getCategories<TResult = any>({
    parentId,
  }: {
    parentId: string
  }): Promise<TResult> {
    return this.get("catalog/categories", { parent_id: parentId })
  }

  async getProduct<TResult = any>({
    id,
    include,
  }: SingleProduct): Promise<TResult> {
    const params: URLSearchParamsInit = {
      ...(include ? { include: include.join(",") } : {}),
    }

    const res = await this.get(`catalog/products/${id}`, params)

    return res
  }

  async getBrands<TResult = any>(): Promise<TResult> {
    const res = await this.get(`catalog/brands`)

    return res
  }

  async getBrand<TResult = any>({
    brandId,
  }: {
    brandId: number
  }): Promise<TResult> {
    const res = await this.get(`catalog/brands/${brandId}`)

    return res
  }

  // async createAccount<TResult = any>({ account }: Account): Promise<TResult> {
  //   const res = await this.post(`/customers`, [{ ...account }])

  //   return res
  // }

  // Allow customer to add an address to their account for faster checkout next
  // time, but don't require it when signing up
  // async createAccountAddress<TResult = any>({ address }): Promise<TResult> {
  //   const res = await this.post(`/customers/addresses`, [{ ...address }])

  //   return res
  // }

  async getAllCustomers<TResult = any>({ email }): Promise<TResult> {
    const params: URLSearchParamsInit = {
      "email:in": email,
    }

    const res = await this.get("/customers", params)

    return res
  }

  // NOTE: This uses v2 endpoint, so make sure to use `bcAPIv2`
  async getTime<TResult = any>(): Promise<TResult> {
    const res = await this.get("/time")

    return res
  }
}
