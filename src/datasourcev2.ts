import { RequestOptions, RESTDataSource } from "apollo-datasource-rest"
import { parseString } from "xml2js"

import config from "config"

export class BigCommerceAPIV2 extends RESTDataSource {
  constructor(baseUrl: string) {
    super()
    this.baseURL = baseUrl
  }

  willSendRequest(req: RequestOptions): void {
    req.headers.set("x-auth-token", config.get("token"))
  }

  async getTime(): Promise<{ data: { time: number } }> {
    let time = null
    const res = await this.get("/time")

    parseString(res, (err: any, res: any) => {
      if (!err) {
        time = res.time.time[0]
      }
    })

    return {
      data: time,
    }
  }
}
