import { isPlainObject, mapKeys, mapValues } from "lodash"

type AnyObject = { [k: string]: any }
type Callback = (value: string, key: string) => any

export default function mapKeysDeep(
  obj: AnyObject,
  cb: Callback,
  isRecursive?: boolean,
) {
  if (!obj && !isRecursive) {
    return {}
  }

  if (!isRecursive) {
    if (
      typeof obj === "string" ||
      typeof obj === "number" ||
      typeof obj === "boolean"
    ) {
      return {}
    }
  }

  if (Array.isArray(obj)) {
    return obj.map((item) => mapKeysDeep(item, cb, true))
  }

  if (!isPlainObject(obj)) {
    return obj
  }

  const result = mapKeys(obj, cb)

  return mapValues(result, (value) => mapKeysDeep(value, cb, true))
}
