export enum Categories {
  FEATURED = 75,
}

export enum ELiquidFilters {
  STRENGTH = 38,
  SIZES = 46,
  FLAVOR = 34,
}
