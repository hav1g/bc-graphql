/*
 * NOTE:
 * For future self: this exists as its own data source because we want to
 * be able to _always_ grab the different options for a product, regardless of
 * the request method, when operating on a cart. We do this so we can
 * differentiate variants of products. E.g. If I select a strength of "3mg" but
 * also select "6mg" for the same product, the cart would show "Product name",
 * but not the strength options. This is bad user experience. Instead, with this,
 * we can combine `productName` and `variantName` to display the differences.
 *
 * If this were to live in `BigCommerceAPI`, we would always
 * be adding "line_items.physical_items.options", but not every endpoint
 * supports this.
 *
 * ETA: :point_up: is retarded reasoning. We can just check the `req.path` to
 * decide how to add `req.params`, but the organization here is still superior to
 * throwing everything into a single data source.
 */
import { RequestOptions, RESTDataSource } from "apollo-datasource-rest"

import config from "config"
import { UpdateOperation } from "./lib/constants"

interface Error {
  data: {
    code: number
    title: string
  }
}

interface Cart {
  cartId?: string
  lineItems: Array<{
    quantity: number
    productId: number
    variantId: number
  }>
}

export class CartAPI extends RESTDataSource {
  constructor(baseUrl: string) {
    super()
    this.baseURL = baseUrl
  }

  willSendRequest(req: RequestOptions) {
    req.headers.set("x-auth-token", config.get("token"))
    req.params.set("include", "line_items.physical_items.options")
  }

  async createCart<TResult = any>({ lineItems }: Cart): Promise<TResult> {
    const res = await this.post(`/carts`, { line_items: lineItems })

    return res
  }

  async updateCart<TResult = any>(
    operation: UpdateOperation,
    { cartId, lineItems }: Cart,
  ): Promise<TResult> {
    let res: TResult

    if (operation === UpdateOperation.UPDATE) {
      res = await this.post(`/carts/${cartId}/items`, {
        line_items: lineItems,
      })
    }

    if (operation === UpdateOperation.DELETE) {
      res = await this.delete(`/carts/${cartId}`)
    }

    return res
  }

  async getCart<TResult = any>({ cartId }: Cart): Promise<TResult | Error> {
    let res: TResult | Error = null

    try {
      res = await this.get(`/carts/${cartId}`)
    } catch (e) {
      console.log(e.extensions.response)
      const { status, body } = e.extensions.response

      res = {
        data: {
          code: status,
          title: JSON.parse(body).status,
        },
      }
    }

    return res
  }

  async updateItemInCart<TResult = any>(
    operation: UpdateOperation,
    { cartId, lineItem },
  ): Promise<TResult | Error> {
    let res: TResult | Error = null

    try {
      if (operation === UpdateOperation.UPDATE) {
        const { product_id, quantity, id } = lineItem

        res = await this.put(`/carts/${cartId}/items/${id}`, {
          line_item: {
            product_id,
            quantity: quantity,
          },
        })
      }

      if (operation === UpdateOperation.DELETE) {
        const { id } = lineItem

        res = await this.delete(`/carts/${cartId}/items/${id}`)
      }
    } catch (e) {
      const { status, body } = e.extensions.response

      res = {
        data: {
          code: status,
          title: JSON.parse(body).title,
        },
      }
    }

    return res
  }

  async createCheckoutRedirects<TResult = any>({ cartId }): Promise<TResult> {
    const res = await this.post(`/carts/${cartId}/redirect_urls`)

    return res
  }
}
