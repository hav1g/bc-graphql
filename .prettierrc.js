module.exports = {
  semi: false,
  prinWidth: 80,
  singleQuote: false,
  trailingComma: "all",
  bracketSpacing: true,
}
